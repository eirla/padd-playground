create or replace package pkg_PADD is

  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype DocObjectList is extension.PADDDocObjectlist;

  /*---------------------------------------------------------------------------
   * GetDocsForUpload()
   *   Determine the documents that need to be uploaded to PADD.
   *-------------------------------------------------------------------------*/
  function GetDocsForUpload return DocObjectList;

  /*---------------------------------------------------------------------------
   * LogDMSEvent()
   *   Log an event in the Extension.PADDLog_t table.
   *-------------------------------------------------------------------------*/
  procedure LogDMSEvent (
    a_Action                            varchar2,
    a_ExceptionDescription              varchar2,
    a_DocumentId                        udt_id,
    a_DocRevisionNum                    number,
    a_RelatedJobId                      udt_id,
    a_PADDErrorCode                     varchar2,
    a_ErrorStackTrace                   varchar2
  );

  /*---------------------------------------------------------------------------
   * PrepareForDelete()
   *   Ensure that the cascade delete works on the documents when a document is
   * deleted and both the POSSE Document Object and PADD File are removed.
   *   Switch revisions on a document to be for the default management scheme
   * and schedule removal of the external document by the process server.
   *-------------------------------------------------------------------------*/
  procedure PrepareForDelete (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
 );

  /*---------------------------------------------------------------------------
   * ResetDocumentFailureCount()
   *   Reset the Tries count on the PADDUploadFailures_t table for either a
   * single document or the whole table. For a single document pass a_DocumentId.
   *-------------------------------------------------------------------------*/
  procedure ResetDocumentFailureCount (
    a_DocumentId                        number
  );

  procedure ResetDocumentFailureCount;

  /*---------------------------------------------------------------------------
   * SetUploadtoPADD()
   *   Enable the Word Merge Document type to be moved to PADD after it has been
   * Signed & Saved.
   *-------------------------------------------------------------------------*/
  procedure SetUploadtoPADD (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

end pkg_PADD;

/

grant execute
on pkg_padd
to posseextensions;

create or replace package body pkg_PADD is

  subtype DocObject is extension.PADDDocObject;
  subtype udt_IdList is api.pkg_definition.udt_IdList;
  subtype udt_ObjectList is api.udt_ObjectList;

  /*---------------------------------------------------------------------------
   * GetDocsForUpload()
   *-------------------------------------------------------------------------*/
  function GetDocsForUpload return DocObjectList is
    t_DocumentTypeId                    udt_Id;
    t_SystemSettingsId                  udt_Id;
    t_MaxAttempts                       number;
    t_MaxRows                           number;
    t_OlderThanDate                     date;
    t_PADDDocRetentionDays              number;
    t_TempList                          DocObjectList;
  begin

    t_TempList := extension.PADDDocObjectList();

    t_SystemSettingsId := api.pkg_SimpleSearch.ObjectByIndex(
        'o_SystemSettings', 'SystemSettingsNumber', 1);

    --IF USING SYSTEM SETTINGS, use the following code:
    select
      PADDNumberOfDocsToProcess,
      PADDMaxUploadAttempts,
      PADDDocumentRetentionDays
    into
      t_MaxRows,
      t_MaxAttempts,
      t_PaddDocRetentionDays
    from query.o_SystemSettings
    where ObjectId = t_SystemSettingsId;

/*    --IF USING USER CONSTANTS, use the following code:
    t_MaxRows := api.pkg_constantquery.Value('PADDNumberOfDocsToProcess');
    t_MaxAttempts := api.pkg_constantquery.Value('PADDMaxUploadAttempts');
    t_PADDDocRetentionDays := nvl( api.pkg_constantquery.Value('PADDDocumentRetentionDays'), 0);*/

    t_OlderThanDate := sysdate - t_PaddDocRetentionDays;

    -- Get documents NOT in PADD and are older than t_PADDDocRetentionDays
    select extension.PADDDocObject(r.DocumentId, r.RevisionNum)
    bulk collect into t_TempList
    from
      doc.DocumentRevisions r
      join api.logicaltransactions lt
          on lt.LogicalTransactionId = r.LogicalTransactionId
      join api.Documents d
          on d.DocumentId = r.DocumentId
    where (
        r.ManagementSchemeId not in (
          -- API BREAK
          -- Get the DMS id for PADD
          select ManagementSchemeId
          from stage.DocumentManagementSchemes
          where name in (
            'PADDFileSystem_v3',
            'PADDAzureBlobs'
          )
        )
        or r.ManagementSchemeId is null
      )
      and lt.CreatedDate <= t_OlderThanDate
      and (
        'N' not in (
          select dtc.UploadToPADD
          from extension.PADDDocumentTypeConfig dtc
          where dtc.DocumentTypeId = d.DocumentTypeId
        )
        or r.DocumentId in (
          select DocumentId
          from extension.PADDDocumentUploadQueue
        )
      )
      and (
        select nvl(max(tries), 0)
        from extension.PADDUploadFailures_t f
        where f.Document_Id = r.DocumentId
          and f.Doc_Revision_Num = r.RevisionNum
      ) < t_MaxAttempts
      and ROWNUM <= t_MaxRows;

    return t_TempList;

  end GetDocsForUpload;

  /*---------------------------------------------------------------------------
   * LogDMSEvent
   *-------------------------------------------------------------------------*/
  procedure LogDMSEvent(
    a_Action                            varchar2,
    a_ExceptionDescription              varchar2,
    a_DocumentId                        udt_id,
    a_DocRevisionNum                    number,
    a_RelatedJobId                      udt_id,
    a_PADDErrorCode                     varchar2,
    a_ErrorStackTrace                   varchar2
  ) is
    PRAGMA AUTONOMOUS_TRANSACTION;
    t_tries number;
    t_Seq   number;
  begin

    t_Seq := extension.PADDLog_s.nextval();
    insert into extension.PADDLog_t (
      log_id,
      log_date,
      action,
      exception_description,
      document_id,
      doc_revision_num,
      related_job_id,
      error_code,
      error_stack_trace
    ) values (
      t_Seq,
      systimestamp,
      a_Action,
      a_ExceptionDescription,
      a_DocumentId,
      a_DocRevisionNum,
      a_RelatedJobId,
      a_PADDErrorCode,
      a_ErrorStackTrace);

    if a_Action = 'Set' and a_ExceptionDescription = 'Failed' then
      begin
        select tries
        into t_tries
        from extension.PADDUploadFailures_t d
        where d.document_id = a_DocumentId
          and d.doc_revision_num = a_DocRevisionNum;

        update extension.PADDUploadFailures_t d set
          tries = t_tries + 1
        where d.Document_Id = a_DocumentId
          and d.doc_revision_num = a_DocRevisionNum;
      exception when no_data_found then
        t_tries := 0;

      insert into extension.PADDUploadFailures_t (
        log_date,
        document_id,
        doc_revision_num,
        related_job_id,
        tries
      ) values (
        systimestamp,
        a_DocumentId,
        a_DocRevisionNum,
        a_RelatedJobId,
        t_Tries + 1);
      end;

    elsif a_Action = 'Set' and a_ExceptionDescription = 'Succeeded' then
      begin
        delete from extension.PADDUploadFailures_t d
        where d.document_id = a_DocumentId
          and d.doc_revision_num = a_DocRevisionNum;
      exception when no_data_found then
        null;
      end;
    end if;

    commit;

  end LogDMSEvent;

  /*---------------------------------------------------------------------------
   * PrepareForDelete() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure PrepareForDelete (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_IsDummyDocument                   char(1);
    t_ScheduleId                        number;
    t_ScriptDescription                 varchar2(4000) := 'PADD - Remove Deleted Documents';
  begin

    select decode(max(TailRevisionBaseName), 'PADDRemoveDeletedDocsDummy', 'Y', 'N')
    into t_IsDummyDocument
    from api.Documents
    where DocumentId = a_ObjectId;

    if t_IsDummyDocument = 'N' then
      -- API BREAK HERE
      for p in (
          select
            Location,
            RevisionNum
          from doc.DocumentRevisions
          where DocumentId = a_ObjectId
            and ManagementSchemeId is not null
          order by RevisionNum
          ) loop

        begin
          insert into PADDDeletedDocuments (
            DocumentId,
            RevisionNum,
            Location
          ) values (
            a_ObjectId,
            p.RevisionNum,
            p.Location
          );
        exception
          when dup_val_on_index then
            update PADDDeletedDocuments set
              Location = p.Location
            where DocumentId = a_ObjectId
              and RevisionNum = p.RevisionNum;
        end;

      end loop;
    end if;

    -- API BREAK HERE - set all of the revisions to the default management scheme
    update doc.DocumentRevisions set
      ManagementSchemeId = null
    where DocumentId = a_ObjectId;

    if t_IsDummyDocument = 'N' then
      -- Schedule clean-up script
      select max(ScheduleId)
      into t_ScheduleId
      from api.ScheduledItems
      where Description = t_ScriptDescription;

      if t_ScheduleId is null then
        t_ScheduleId := api.pkg_ProcessServer.ScheduleTrustedPythonScript(
            'PADDRemoveDeletedDocs',
            t_ScriptDescription,
            a_Priority => api.pkg_Definition.gc_Low
        );
      end if;
    end if;

 end PrepareForDelete;

  /*---------------------------------------------------------------------------
   * ResetDocumentFailureCount()
   *-------------------------------------------------------------------------*/
  procedure ResetDocumentFailureCount (
    a_DocumentId                        number
  ) is

  begin

    update extension.PADDUploadFailures_t f set
      f.tries = 0
    where f.document_id = a_DocumentId;

    commit;

  end ResetDocumentFailureCount;

  /*---------------------------------------------------------------------------
   * ResetDocumentFailureCount()
   *-------------------------------------------------------------------------*/
  procedure ResetDocumentFailureCount is

  begin

    update extension.PADDUploadFailures_t f set
      f.tries = 0;

    commit;

  end ResetDocumentFailureCount;


  /*---------------------------------------------------------------------------
   * SetUploadtoPADD()
   *-------------------------------------------------------------------------*/
  procedure SetUploadtoPADD(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_ExtensionType                     varchar2(20);
    t_Count                             number;
  begin

    select tailrevisionextension
    into t_ExtensionType
    from api.documents
    where documentid = a_ObjectId;

    if t_ExtensionType = 'docm' then
      return;
    end if;

    select count(1)
    into t_Count
    from extension.PADDDocumentUploadQueue
    where DocumentId = a_ObjectId;

    if t_Count > 0 then
      return;
    end if;

    insert into extension.PADDDocumentUploadQueue (
      DocumentId
    ) values (
      a_ObjectId
    );

  end SetUploadtoPADD;

end pkg_PADD;

/

