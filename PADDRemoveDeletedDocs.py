"""
Removes files from PADD when documents have been deleted via database procedures
or scripts.
"""
import cx_Logging
import requests
import urlparse


# Document Type must be associated with the DEFAULT storage scheme
DUMMY_DOCUMENT_DEF_NAME = "d_ElectronicDocument"


class DocumentDeleterBase(object):
    DMS_NAME = None
    WEB_SERVICE_URL_COLUMN_NAME = None

    __CachedDocumentManagementScheme = None
    __CachedWebServiceURL = None

    @classmethod
    def __GetDocumentManagementScheme(cls):
        global dataArea

        if not cls.__CachedDocumentManagementScheme:
            cls.__CachedDocumentManagementScheme = (
                dataArea.configCache.DocManagementSchemeForName(cls.DMS_NAME)
            )

        return cls.__CachedDocumentManagementScheme

    @classmethod
    def __GetWebServiceURL(cls):
        global dataArea

        if not cls.__CachedWebServiceURL:
            systemSettings = dataArea.UniqueSearch("o_SystemSettings")
            cls.__CachedWebServiceURL = systemSettings[cls.WEB_SERVICE_URL_COLUMN_NAME]

        return cls.__CachedWebServiceURL

    @classmethod
    def __DocumentExists(cls, revisionLocation):
        webServiceURL = cls.__GetWebServiceURL()
        existsPath = urlparse.urljoin(webServiceURL, "exists", revisionLocation)
        response = requests.get(existsPath, verify=False)

        return response.status_code == 200 and response.json()["exists"]

    @classmethod
    def DeleteRevision(cls, revisionLocation):
        global dataArea

        if not cls.__DocumentExists(revisionLocation):
            cx_Logging.Info("Document revision {} not found.".format(revisionLocation))
            return

        managementScheme = cls.__GetDocumentManagementScheme()

        cx_Logging.Info(
            "Removing document revision {} using {}".format(
                revisionLocation, managementScheme.name
            )
        )

        dummyDocumentId = None

        try:
            # To make sure all deletes from the database get run through the
            # PADD document management schemes, we want to upload a dummy document
            # and attempt to remove it through the management schemes.
            dummyDocument = dataArea.CreateObject(DUMMY_DOCUMENT_DEF_NAME)
            cx_Logging.Info("Created dummy document.")
            dummyDocument.SetBlob("dummy")
            cx_Logging.Info("Uploaded dummy blob.")
            dummyRevision = dummyDocument.GetRevision()
            cx_Logging.Info("Retrieved dummy revision: " + str(dummyRevision))
            dummyRevision.baseName = "PADDRemoveDeletedDocsDummy"
            dummyRevision.RegisterLocationChange(revisionLocation, managementScheme)
            cx_Logging.Info("Changed dummy revision location.")
            dataArea.Update()
            cx_Logging.Info("Saved dummy document: " + str(dummyDocument.objectId))
            dummyDocumentId = dummyDocument.objectId
            dataArea.Clear()
            dummyDocument = dataArea.ObjectForId(dummyDocument.objectId)
            cx_Logging.Info("Retrieved saved dummy document: " + str(dummyDocument))
            dummyDocument.Remove()
            dataArea.Update()
            cx_Logging.Info("Removed document revision.")
            dataArea.Clear()
            return
        except Exception:
            cx_Logging.Warning(
                "Unable to remove document revision {}".format(revisionLocation)
            )
            cx_Logging.LogException()
            dataArea.Clear()

        # To avoid leaving empty documents around, remove the record from the database.
        try:
            dataArea.ExecuteSql("PADDDeleteDummyDocument", DocumentId=dummyDocumentId)
            dataArea.Update()
            dataArea.Clear()
            cx_Logging.Info("Removed dummy document.")
        except Exception as e:
            raise Exception("Failed to remove dummy document: {}".format(e))


class FileSystemDeleter(DocumentDeleterBase):
    DMS_NAME = "PADDFileSystem_v3"
    WEB_SERVICE_URL_COLUMN_NAME = "PADDFileSystemWebServiceUrl"


class AzureDeleter(DocumentDeleterBase):
    DMS_NAME = "PADDAzureBlobs"
    WEB_SERVICE_URL_COLUMN_NAME = "PADDAzureBlobWebServiceUrl"


def RemoveDocumentFromQueue(documentId):
    global dataArea
    dataArea.ExecuteSql("PADDRemoveDeletedDoc", DocumentId=documentId)


def Main():
    global dataArea

    for (documentId,) in dataArea.ExecuteSql("PADDGetDeletedDocs"):
        try:
            for (revisionNum, location) in dataArea.ExecuteSql(
                "PADDGetDeletedDocRevs", DocumentId=documentId
            ):
                FileSystemDeleter.DeleteRevision(location)
                AzureDeleter.DeleteRevision(location)

            RemoveDocumentFromQueue(documentId)
            dataArea.Update()
            dataArea.Clear()
        except Exception:
            cx_Logging.Error(
                "An error occurred when removing document: {}".format(documentId)
            )
            cx_Logging.LogException()


Main()
